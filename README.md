# Branching Strategy

This repo has the following hypothetical branching strategy:

1. The `main` branch is protected and only Maintainers can merge other branches into it via th GitLab UI.
1. Branches starting with `feat/*` or `fix/*` are protected, but Developers and Maintainers are free to push them.
1. All other branch names are forbidden (No one can merge or push).

## Configuration

![](Protected_branches.png)
